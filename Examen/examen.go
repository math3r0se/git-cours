package main

import ( //Import des librairies
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

type Task struct { //Structure Task
	Description string
	Done        bool
}
type List struct { //Structure List pour func list()
	ID   string
	Task string
}

var tasks []Task

func list(rw http.ResponseWriter, _ *http.Request) {
	tasklist := []List{}

	tasks = []Task{ //Injection dans le "tasks"
		{"Faire les courses", false},
		{"Payer les factures", false},
	}

	for id, i := range tasks {
		if !i.Done { //Si tasks.Done == False
			tasklist = append(tasklist, List{strconv.Itoa(id), i.Description}) //Créer entrer dans List
		}
	}

	rw.WriteHeader(http.StatusOK) //Envoyer un Status OK
	data, _ := json.Marshal(list)

	rw.Write(data) //Ecrire la list dans la page
	return
}
func add(rw http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost { //Si méthode POST
		rw.WriteHeader(http.StatusBadRequest) //Bad request
	} else {
		rw.WriteHeader(http.StatusOK) //Sinon, Status OK
	}

	body, err := ioutil.ReadAll(r.Body) //Lecture du Body
	if err != nil {
		fmt.Printf("Error reading body: %v", err)
		http.Error(
			rw,
			"can't read body", http.StatusBadRequest,
		)
		return
	}
	description := string(body)                     //Récupération du body dans un string pour "Description"
	tasks = append(tasks, Task{description, false}) //Ajout Body dans "tasks"
	rw.WriteHeader(http.StatusOK)                   //Envoyer un Status OK
}
func done(rw http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet: //Si méthode GET
	case http.MethodPost: //Si méthode POST
	default:
		rw.WriteHeader(http.StatusBadRequest) //Sinon, Bad request
	}
}
func main() {

	http.HandleFunc("/", list)                   //Donner chemin de la func list pour localhost:8080/list
	http.HandleFunc("/done", done)               //Donner chemin de la func done pour localhost:8080/done
	http.HandleFunc("/add", add)                 //Donner chemin de la func add pour localhost:8080/add
	log.Fatal(http.ListenAndServe(":8080", nil)) //Ouverture du serveur sur le post 8080

}

// Joël Tordjman - MSI DevOps groupe A
// Examen GoLang du 12/02/2021
