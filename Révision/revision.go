package main

type Task struct {
	Description string
	Done        bool
}

var task := []Task{}

func main() {
	// Fonctions 
	list := func list(){}	
	path := func done(){}
	add := func add(){}

	http.HandleFunc("/", list)
	http.HandleFunc("/done", done)
	http.HandleFunc("/add", add)
}
