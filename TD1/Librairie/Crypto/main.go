package main

import (
	//"crypto/sha256"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func getbytesize(path string) []byte{
	content, err := os.Open(path)
	if err != nil {
		log.Panicf("failed reading file: %s", err)
	}
	defer content.Close()
	data, err := ioutil.ReadAll(content)

	return data
}

func main() {
	// Return file size in bytes
	var img1 = getbytesize("image_1.jpg")
	var img2 = getbytesize("image_2.jpg")
	var img3 = getbytesize("image_3.jpg")

	fmt.Printf("\nImage 1: %d bytes", len(img1))
	fmt.Printf("\nImage 2: %d bytes", len(img2))
	fmt.Printf("\nImage 3: %d bytes", len(img3))
	// End

}