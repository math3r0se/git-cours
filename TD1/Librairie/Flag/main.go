package main

import (
	"flag"
	"fmt"
)

const Version = "1.0"

func main() {
	ver := flag.Bool("version", false, "Show version")
	flag.Parse()
	if *ver {
		fmt.Println(Version)
	}else {
		fmt.Println("Salut")
	}
}