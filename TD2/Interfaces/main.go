package main

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

type IPAddr [4]byte

/*func (ip IPAddr) String() string {									// With FMT
	return fmt.Sprintf("%d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3])
}*/

func (ip IPAddr) String() string { // With STRCONV
	s := make([]string, len(ip))
	for i, val := range ip {
		s[i] = strconv.Itoa(int(val))
	}
	return strings.Join(s, ".")
}

type MyError struct {
	When time.Time
	What string
}

type Error interface {
}

func run() MyError {
	fail := MyError{
		When: time.Now(),
		What: "Error"}
	return fail
}

func PrintIt(input interface{}) {
	Println(input)
}

func main() {
	err := run()

	hosts := map[string]IPAddr{
		"loopback":  {127, 0, 0, 1},
		"googleDNS": {8, 8, 8, 8},
	}

	for name, ip := range hosts {
		fmt.Printf("%v:%v\n", name, ip)
	}

	Println(PrintIt("Salut"))

}

func fn0(err MyError) {
	if err != nil {
		fmt.Println(err)
	}
}
