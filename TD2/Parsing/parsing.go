package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

func CheckError(e error) {
	if e != nil {
		panic(e)
	}
}

type User struct {
	Login    string `json:"userName"`
	Password string
}

func main() {

	var data, err = ioutil.ReadFile("users.json")
	CheckError(err)

	var results []map[string]User

	json.Unmarshal([]byte(data), &results)

	for key, results := range results {
		fmt.Println("Valeur: ", key)
		fmt.Println("Id: ", results["id"])
	}
}
