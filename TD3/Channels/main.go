package main

import (
	"fmt"
	"time"
)

type Response struct {
	respText string
	err      error
}

func callServer(address string, ch chan Response) {
	time.Sleep(2 * time.Second)
	ch <- Response{}
}

func main() {

	//--------------

	ch1 := make(chan Response)
	ch2 := make(chan Response)

	//--------------

	go callServer("adr", ch1)
	go callServer("add", ch2)

	//--------------

	select {
	case <-ch1:
		fmt.Println("channel 1")
	case <-ch2:
		fmt.Println("channel 2")
	}

	//--------------

	fmt.Println("Après")

	//--------------

}
